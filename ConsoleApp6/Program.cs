﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Addressbook
{
    class NoteBook
    {
        static void Main(string[] args)
        
        {
            Console.WriteLine("Здравствуйте!");

            Dictionary<int,Note> a = new Dictionary<int, Note>();

            Console.WriteLine("Введите число от 1 до 5");
            Console.WriteLine("1) Создания новой записи");
            Console.WriteLine("2) Редактирования созданных записей.");
            Console.WriteLine("3) Удаления созданных записей.");
            Console.WriteLine("4) Просмотра созданных учетных записей.");
            Console.WriteLine("5) Просмотра всех созданных учетных записей, с краткой информацией");
            
            while (true)
            {
                int i = Convert.ToInt32(Console.ReadLine());
                Note person = new Note();
                functional b = new functional();
                switch (i)
                {
                    case 1:
                        b.CreateNewNote(a, person);
                        break;
                    case 2:
                        b.EditNote(a, person);
                        break;
                    case 3:
                        b.DeleteNote(a, person);
                        break;
                    case 4:
                        b.ReadNote(a);
                        break;
                    case 5:
                        b.ShowAllNotes(a);
                        break;
                }
            }          
        }
    }
    public class Note
    {
        static int count = 0;
        public Note() { this.id = Note.count;Note.count += 1; }
        public int id ;
        public string familia;
        public string name;
        public string otchestvo;
        public int number;
        public string country;
        public DateTime birthsday;
        public string organisation;
        public string dolznost;
        public string other;
        public override string ToString()
        {
            return "Фамилия " + this.familia + " имя " + this.name + " отчество " + this.otchestvo + " номер телефона " + this.number + " страна " + this.country + " дата рождения " + this.birthsday + " организация " + this.organisation + " должность " + this.dolznost + " прочие записи " + this.other;
        }
    }
    public class functional
    {
        public void CreateNewNote(Dictionary<int, Note> a, Note person)
        {
            Console.WriteLine("Введите фамилию");
            person.familia = Console.ReadLine();
            while (person.familia.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.familia = Console.ReadLine();
            }
            Console.WriteLine("Введите имя");
            person.name = Console.ReadLine();
            while (person.name.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.name = Console.ReadLine();
            }
            Console.WriteLine("Введите отчество");
            person.otchestvo = Console.ReadLine();
            Console.WriteLine("Введите номер телефона");
            person.number = Convert.ToInt32(Console.ReadLine());
            while (person.number.ToString().Length < 5)
            {
                Console.WriteLine("Вы ввели некоректно");
                
                person.number = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("Введите страну");
            person.country = Console.ReadLine();
            while (person.country.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
               
                person.country = Console.ReadLine();
            }
            Console.WriteLine("Введите дату рождения");
            person.birthsday = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Введите организацию");
            person.organisation = Console.ReadLine();
            Console.WriteLine("Введите должность");
            person.dolznost = Console.ReadLine();
            Console.WriteLine("Введите прочие заметки");
            person.other = Console.ReadLine();
            a.Add(person.id, person);
              
        }
        public void EditNote(Dictionary<int, Note> a, Note person)
        {
            Console.WriteLine("Введите запись которую хотите поменять");
            person.id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите фамилию");
            person.familia = Console.ReadLine();
            while (person.familia.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.familia = Console.ReadLine();
            }
            Console.WriteLine("Введите имя");
            person.name = Console.ReadLine();
            while (person.name.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.name = Console.ReadLine();
            }
            Console.WriteLine("Введите отчество");
            person.otchestvo = Console.ReadLine();
            Console.WriteLine("Введите номер телефона");
            
            person.number = Convert.ToInt32(Console.ReadLine());
            while (person.number.ToString().Length < 5)
            {
                Console.WriteLine("Вы ввели некоректно");
                person.number = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("Введите страну");
            person.country = Console.ReadLine();
            while (person.country.Length == 0)
            {
                Console.WriteLine("Вы ничего не ввели");
                person.country = Console.ReadLine();
            }
            Console.WriteLine("Введите дату рождения");
            person.birthsday = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Введите организацию");
            person.organisation = Console.ReadLine();
            Console.WriteLine("Введите должность");
            person.dolznost = Console.ReadLine();
            Console.WriteLine("Введиие прочие заметки");
            person.other = Console.ReadLine();
            a.Remove(person.id);
            a.Add(person.id, person);
        }
        public void DeleteNote(Dictionary<int, Note> a, Note person)
        {
            Console.WriteLine("Введите запись которую хотите удалить");
            person.id = Convert.ToInt32(Console.ReadLine());
            a.Remove(person.id);
        }
        public void ReadNote(Dictionary<int, Note> a)
        {
            foreach (KeyValuePair<int, Note> box in a)
            {
                Console.WriteLine(box.Key + " " + box.Value.ToString());
            }
        } 
        public void ShowAllNotes(Dictionary<int, Note> a)
        {
            foreach (KeyValuePair<int, Note> box in a)
            {
                Console.WriteLine("Id = {0}, Фамилия = {1}, Имя = {2}, Номер телефона = {3}", box.Key, box.Value.familia, box.Value.name, box.Value.number);
            }
        }
        
        }
    }

